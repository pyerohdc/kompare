package com.gitlab.pyerohdc.kompare

/**
 * Komparer for uncomparable objects
 */
class UnKompare<T : Any> private constructor(override val value: T?, private val comparator: Comparator<T>) :
    BaseKompare<T> {
    companion object {
        fun <T : Any> of(value: T?, comparator: Comparator<T>): UnKompare<T> = UnKompare(value, comparator)
    }

    private val comparableValue = ComparableWrapper(value, comparator)

    override fun isLowerThan(value: T?) =
        this.value != null && value != null && comparableValue < value

    override fun isLowerThanOrEqual(value: T?) =
        (this.value == null && value == null) || this.value != null && value != null && comparableValue <= value

    override fun isGreaterThan(value: T?) =
        this.value != null && value != null && comparableValue > value

    override fun isGreaterThanOrEqual(value: T?) =
        (this.value == null && value == null) || this.value != null && value != null && comparableValue >= value
}
