package com.gitlab.pyerohdc.kompare

interface BaseKompare<T : Any> {
    val value: T?

    /**
     * Check if the value is null
     */
    fun isNull() = value == null

    /**
     * Check if the value is not null
     */
    fun isNotNull() = !isNull()

    /**
     * Check if the two values are equal
     */
    infix fun isEqualTo(value: T?) = this.value == value

    /**
     * [isEqualTo]
     */
    infix fun eq(value: T?) = isEqualTo(value)

    /**
     * Check if the two values are **not** equal
     */
    infix fun isNotEqualTo(value: T?) = this.value != value

    /**
     * [isNotEqualTo]
     */
    infix fun neq(value: T?) = isNotEqualTo(value)

    /**
     * Check if the value is strictly lower than the hold value
     */
    infix fun isLowerThan(value: T?): Boolean

    /**
     * [isLowerThan]
     */
    infix fun lt(value: T?) = isLowerThan(value)

    /**
     * Check if the value is lower than the hold value, or equal
     */
    infix fun isLowerThanOrEqual(value: T?): Boolean

    /**
     * [isLowerThanOrEqual]
     */
    infix fun lte(value: T?) = isLowerThanOrEqual(value)

    /**
     * Check if the value is strictly greater than the hold value
     */
    infix fun isGreaterThan(value: T?): Boolean

    /**
     * [isGreaterThan]
     */
    infix fun gt(value: T?) = isGreaterThan(value)

    /**
     * Check if the value is greater than the hold value, or equal
     */
    infix fun isGreaterThanOrEqual(value: T?): Boolean

    /**
     * [isGreaterThanOrEqual]
     */
    infix fun gte(value: T?) = isGreaterThanOrEqual(value)
}
