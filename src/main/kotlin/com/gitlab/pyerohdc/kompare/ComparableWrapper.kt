package com.gitlab.pyerohdc.kompare

class ComparableWrapper<T>(
    private val value: T?,
    private val comparator: Comparator<T>
) : Comparable<T> {
    override fun compareTo(other: T): Int {
        return comparator.compare(value, other)
    }
}
