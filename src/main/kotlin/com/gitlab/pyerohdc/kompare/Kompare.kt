package com.gitlab.pyerohdc.kompare

import java.math.BigDecimal
import java.math.BigInteger
import kotlin.reflect.KClass

/**
 * Komparer for comparable objects
 */
class Kompare<T : Comparable<T>> private constructor(
    override val value: T?,
    val zeroValues: Map<out KClass<out Any>, Any> = defaultZeroValues
) : BaseKompare<T> {
    companion object {
        val defaultZeroValues = mapOf(
            Int::class to 0,
            Long::class to 0L,
            Float::class to 0f,
            Double::class to 0.0,
            Short::class to 0.toShort(),
            Byte::class to 0.toByte(),
            BigInteger::class to BigInteger.ZERO,
            BigDecimal::class to BigDecimal.ZERO
        )

        fun <T : Comparable<T>> of(value: T?): Kompare<T> {
            return Kompare(value)
        }

        fun <T : Comparable<T>> builder(additionalDefaultZeroValues: Map<out KClass<out Any>, Any> = emptyMap()): (T) -> Kompare<T> {
            return { Kompare(it, defaultZeroValues + additionalDefaultZeroValues) }
        }
    }

    override infix fun isLowerThan(value: T?) = this.value != null && value != null && this.value < value

    override infix fun isLowerThanOrEqual(value: T?) =
        (this.value == null && value == null) || this.value != null && value != null && this.value <= value

    override infix fun isGreaterThan(value: T?) = this.value != null && value != null && this.value > value

    override infix fun isGreaterThanOrEqual(value: T?) =
        (this.value == null && value == null) || this.value != null && value != null && this.value >= value
}

/**
 * Check if the value is zero. Works only with basic number types (Java primitive numbers, BigDecimal and BigInteger)
 */
inline fun <reified T> Kompare<T>.isZero(): Boolean
    where T : Number,
          T : Comparable<T> {
    return value != null && value.compareTo(
        (zeroValues[T::class] ?: throw IllegalArgumentException("Unsupported number type : ${T::class}")) as? T
            ?: error("${T::class} should have a proper corresponding zero value !")
    ) == 0
}

/**
 * Positive means strictly greater than 0
 */
inline fun <reified T> Kompare<T>.isPositive(): Boolean
    where T : Number,
          T : Comparable<T> {
    return value != null && (
        value > (
            (zeroValues[T::class] ?: throw IllegalArgumentException("Unsupported number type : ${T::class}")) as? T
                ?: error("${T::class} should have a proper corresponding zero value !")
            )
        )
}

/**
 * Negative means strictly lower than 0
 */
inline fun <reified T> Kompare<T>.isNegative(): Boolean
    where T : Number,
          T : Comparable<T> {
    return value != null && (
        value < (
            (zeroValues[T::class] ?: throw IllegalArgumentException("Unsupported number type : ${T::class}")) as? T
                ?: error("${T::class} should have a proper corresponding zero value !")
            )
        )
}
