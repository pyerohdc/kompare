package com.gitlab.pyerohdc.kompare

import com.gitlab.pyerohdc.kompare.Kompare.Companion.of
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.inspectors.forAll
import io.kotest.matchers.shouldBe
import java.math.BigDecimal
import java.math.BigInteger

class KompareTest : StringSpec({
    "isNull" {
        mapOf(
            null to true,
            0 to false
        ).forAll { (k, v) ->
            of(k).isNull() shouldBe v
        }
    }

    "isNotNull" {
        mapOf(
            0 to true,
            null to false
        ).forAll { (k, v) ->
            of(k).isNotNull() shouldBe v
        }
    }

    "isZero" {
        of(0).isZero() shouldBe true
        of(0L).isZero() shouldBe true
        of(0f).isZero() shouldBe true
        of(0.0).isZero() shouldBe true
        of(0.toShort()).isZero() shouldBe true
        of(0.toByte()).isZero() shouldBe true
        of(BigInteger.ZERO).isZero() shouldBe true
        of(BigDecimal.ZERO).isZero() shouldBe true

        of(null as Int?).isZero() shouldBe false
        of(1).isZero() shouldBe false
    }

    "isPositive" {
        of(1).isPositive() shouldBe true
        of(1L).isPositive() shouldBe true
        of(1f).isPositive() shouldBe true
        of(1.0).isPositive() shouldBe true
        of(1.toShort()).isPositive() shouldBe true
        of(1.toByte()).isPositive() shouldBe true
        of(BigInteger.ONE).isPositive() shouldBe true
        of(BigDecimal.ONE).isPositive() shouldBe true

        of(null as Int?).isPositive() shouldBe false
        of(0).isPositive() shouldBe false
        of(-1).isPositive() shouldBe false
    }

    "isNegative" {
        of(-1).isNegative() shouldBe true
        of(-1L).isNegative() shouldBe true
        of(-1f).isNegative() shouldBe true
        of(-1.0).isNegative() shouldBe true
        of((-1).toShort()).isNegative() shouldBe true
        of((-1).toByte()).isNegative() shouldBe true
        of(-BigInteger.ONE).isNegative() shouldBe true
        of(-BigDecimal.ONE).isNegative() shouldBe true

        of(null as Int?).isNegative() shouldBe false
        of(0).isNegative() shouldBe false
        of(1).isNegative() shouldBe false
    }

    "isEqualTo" {
        forAll(
            row(1, 1, true),
            row(0, null, false),
            row(null, null, true),
            row("", "", true),
            row("Hello world", "Hello world", true),
            row("Hello world", "Hello world !", false)
        ) { v, equalValue, expected ->
            of(v as? Comparable<Any>) eq equalValue as? Comparable<Any> shouldBe expected
        }
    }

    "isNotEqualTo" {
        forAll(
            row(1, 0, true),
            row(0, 1, true),
            row(1, 1, false),
            row(0, null, true),
            row(null, null, false),
            row("", null, true),
            row("", "", false),
            row("Hello world", "Hello world", false),
            row("Hello world", "Hello world !", true)
        ) { v, eq, expected ->
            of(v as? Comparable<Any>) neq eq as? Comparable<Any> shouldBe expected
        }
    }

    "isLowerThan" {
        forAll(
            row(1, 0, false),
            row(0, 1, true),
            row(1, 1, false),
            row(0, null, false),
            row(null, null, false),
            row("", null, false),
            row("", "", false),
            row("Hello world", "Hello world", false),
            row("Hello world", "Hello world !", true)
        ) { v, eq, expected ->
            of(v as? Comparable<Any>) lt eq as? Comparable<Any> shouldBe expected
        }
    }

    "isLowerThanOrEqual" {
        forAll(
            row(1, 0, false),
            row(0, 1, true),
            row(1, 1, true),
            row(0, null, false),
            row(null, null, true),
            row("", null, false),
            row("", "", true),
            row("Hello world", "Hello world", true),
            row("Hello world", "Hello world !", true)
        ) { v, eq, expected ->
            of(v as? Comparable<Any>) lte eq as? Comparable<Any> shouldBe expected
        }
    }

    "isGreaterThan" {
        forAll(
            row(1, 0, true),
            row(0, 1, false),
            row(1, 1, false),
            row(0, null, false),
            row(null, null, false),
            row("", null, false),
            row("", "", false),
            row("Hello world", "Hello world", false),
            row("Hello world", "Hello world !", false),
            row("Hello world !", "Hello world", true)
        ) { v, eq, expected ->
            of(v as? Comparable<Any>) gt eq as? Comparable<Any> shouldBe expected
        }
    }

    "isGreaterThanOrEqual" {
        forAll(
            row(1, 0, true),
            row(0, 1, false),
            row(1, 1, true),
            row(0, null, false),
            row(null, null, true),
            row("", null, false),
            row("", "", true),
            row("Hello world", "Hello world", true),
            row("Hello world", "Hello world !", false),
            row("Hello world !", "Hello world", true)
        ) { v, eq, expected ->
            of(v as? Comparable<Any>) gte eq as? Comparable<Any> shouldBe expected
        }
    }
})
