package com.gitlab.pyerohdc.kompare

import io.kotest.core.config.AbstractProjectConfig
import io.kotest.core.test.AssertionMode

object KotestProjectConfig : AbstractProjectConfig() {
    override val assertionMode = AssertionMode.Error
}
