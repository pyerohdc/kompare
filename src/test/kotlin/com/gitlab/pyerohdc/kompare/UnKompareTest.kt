package com.gitlab.pyerohdc.kompare

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import java.time.LocalDate
import java.time.Month

class UnKompareTest : StringSpec({
    data class CustomDate(val date: LocalDate)

    val old = CustomDate(LocalDate.of(2022, Month.JULY, 15))
    val next = CustomDate(LocalDate.of(2022, Month.DECEMBER, 25))

    val unkomparer = { d: CustomDate? -> UnKompare.of(d, Comparator.comparing { it?.date }) }

    "isLowerThan" {
        forAll(
            row(null, null, false),
            row(null, next, false),
            row(old, null, false),
            row(old, old, false),
            row(old, next, true),
            row(next, old, false)
        ) { v, c, expected ->
            unkomparer(v) lt c shouldBe expected
        }
    }

    "isLowerThanOrEqual" {
        forAll(
            row(null, null, true),
            row(null, next, false),
            row(old, null, false),
            row(old, old, true),
            row(old, next, true),
            row(next, old, false)
        ) { v, c, expected ->
            unkomparer(v) lte c shouldBe expected
        }
    }

    "isGreaterThan" {
        forAll(
            row(null, null, false),
            row(null, next, false),
            row(old, null, false),
            row(old, old, false),
            row(old, next, false),
            row(next, old, true)
        ) { v, c, expected ->
            unkomparer(v) gt c shouldBe expected
        }
    }

    "isGreaterThanOrEqual" {
        forAll(
            row(null, null, true),
            row(null, next, false),
            row(old, null, false),
            row(old, old, true),
            row(old, next, false),
            row(next, old, true)
        ) { v, c, expected ->
            unkomparer(v) gte c shouldBe expected
        }
    }
})
